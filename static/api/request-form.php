<?php
// ini_set('error_reporting', E_ALL);
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);

header('Content-type:application/json;charset=utf-8');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, X-Requested-With");

$json = file_get_contents('php://input');
$req = json_decode($json, true);
$status = false;
$message = null;
$to = 'xensers@gmail.com, web@talecom.ru';
$subject = 'Заявка с сайта talecom.ru';

if ($req['name'] && $req['tel']) {
  $body = $req['message'] ? ' интересуется: <br/><i>«' . $req['message'] . '»</i><br/> и' : '';
  $message =
    '<h3>С сайта TALECOM.RU поступила заявка</h3>'.
    '<p>' . $req['name'] .
    $body .
    ' ждет звонка по номеру: '. $req['tel'] .
    '</p>';

  $headers  = "Content-type: text/html; charset=utf-8 \r\n";
  $headers .= "From: Заявка с сайта talecom.ru <form@talecom.ru>\r\n";
  $headers .= "Reply-To: form@talecom.ru\r\n";

  $status = mail($to, $subject, $message, $headers);
}

$res = [
  'status'  => $status ? 'sussed' : 'error',
  'name'    => $req['name'],
  'tel'     => $req['tel'],
  'message' => $message
];

echo json_encode($res);