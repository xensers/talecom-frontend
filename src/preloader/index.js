function Preloader() { // Class
  this.targets = [
    { key: 'preloaderInit',    val: false  },
    { key: 'DOMContentLoaded', val: false  },
    { key: 'onload',           val: false  },
    { key: 'interactive',      val: false },
    { key: 'complete',         val: false },
    { key: 'mounted',          val: false },
    { key: 'lottie',           val: false },
  ];

  document.documentElement.classList.add('loading');
  this.loading = true;

  document.addEventListener('DOMContentLoaded', function () {
    this.setTarget('DOMContentLoaded', true);
  }.bind(this));

  document.addEventListener('readystatechange', function() {
    this.setTarget(document.readyState, true);
  }.bind(this));

  window.addEventListener('load', function() {
    this.setTarget('onload', true);
  }.bind(this));


  setTimeout(this.end, 15e3);
}

Preloader.prototype.end = function(){
  const elPreloader = document.getElementById('preloader');
  document.documentElement.classList.remove('loading');
  document.documentElement.classList.add('loaded');
  setTimeout(function () {
    elPreloader.style.display = 'none';
  }, 2000);
}

Preloader.prototype.render = function(){
  const elPreloader = document.getElementById('preloader');

  if (elPreloader) {
    const elProgress = elPreloader.getElementsByClassName('preloader__progress-rect')[0];

    requestAnimationFrame(function() {

      const progress = this.getLoadProgress();

      if (progress === 1 || !this.loading) {
        this.loading = false;

        setTimeout(this.end, 2000);
      }

      elProgress.setAttribute('width', progress * 258);

     }.bind(this));
  }
};

Preloader.prototype.setTarget = function(key, val) {
  for(let index in this.targets) {
    if (this.targets.hasOwnProperty(index) && this.targets[index].key === key) {
      let oldVal = this.targets[index].val;
      if (oldVal !== val) {
        console.log(this.targets[index].key, Math.floor(performance.now()));
        this.targets[index].val = val;
        this.render();
      }
    }
  }
};

Preloader.prototype.getLoadProgress = function() {
  let counter = 0;
  for(let target of this.targets) {
     if (target.val) {
       counter++;
     }
  }

  return counter / this.targets.length;
};

window.preloader = new Preloader();
