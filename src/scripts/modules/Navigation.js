import SVG from 'svg.js';
import anime from 'animejs/lib/anime.es';
import SmoothScroll from 'smooth-scroll';
import ScrollStatus from "../libs/ScrollStatus";
const scroll = new SmoothScroll();

class Navigation {
  constructor(el) {
    this.width = 150;
    this.height = 200;
    this.draw = SVG(el).viewbox(0, 0, this.width, this.height);
    this.__activeIndex = 0;
    this.currentAnimation = false;
    this.items = [];
    this.mutedColor = 'rgb(151,170,190)';
    this.strokeWidth = 1.5;
    this.isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);

    document.querySelectorAll('[data-nav-item]').forEach((el, index, nodeList) => {
      const scrollStatus = new ScrollStatus(el);
      const prev = index > 0 ? this.items[index - 1] : false;
      const heightItem = (this.height / nodeList.length) / 2;
      const y  = this.height * ((index+1) / nodeList.length ) - heightItem;
      const x  = 100;
      const y1 = prev ? y - (y - prev.y) / 2: y;
      const x1 = x;
      const title = el.dataset.navItem;
      const color = el.dataset.navColor;

      const text = this.draw.text(title);
      text.font({
        family: 'TTNorms-Bold, sans-serif',
        size: 10
      });
      const pathBG = this.draw.path().attr({
        'stroke-width' : this.strokeWidth
      });
      const path = this.draw.path().attr({
        'stroke-width' : this.strokeWidth
      });
      const dot  = this.draw.path('M9.134 1.498C9.766 1.938 10.264 2.444 10.707 2.956C10.926 3.213 11.128 3.47201 11.31 3.73201C11.401 3.86201 11.487 3.993 11.567 4.124C11.647 4.255 11.722 4.387 11.791 4.519C11.929 4.783 12.042 5.051 12.13 5.326C12.173 5.463 12.211 5.601 12.243 5.743C12.258 5.814 12.273 5.885 12.286 5.957C12.299 6.029 12.31 6.10201 12.321 6.17501C12.401 6.76301 12.407 7.398 12.352 8.157C12.298 8.766 11.862 9.50801 11.225 10.177C10.906 10.512 10.537 10.829 10.141 11.102C9.745 11.375 9.321 11.603 8.892 11.761C8.835 11.787 8.778 11.812 8.72 11.835C8.662 11.858 8.604 11.881 8.545 11.902C8.426 11.944 8.306 11.983 8.183 12.018C7.939 12.089 7.686 12.146 7.431 12.194C6.92 12.288 6.392 12.349 5.899 12.386C4.909 12.46 4.094 12.424 3.93 12.154C3.215 11.506 3.109 10.678 2.837 9.832C2.819 9.779 2.801 9.72701 2.783 9.67401C2.764 9.62201 2.745 9.569 2.724 9.517C2.683 9.412 2.639 9.30801 2.589 9.20501C2.49 8.99801 2.371 8.796 2.228 8.602C1.95 8.212 1.548 7.865 1.176 7.558C1.136 7.545 1.102 7.523 1.074 7.491C1.06 7.475 1.048 7.457 1.037 7.435C1.032 7.425 1.027 7.413 1.022 7.401C1.017 7.388 1.012 7.376 1.008 7.362C0.975003 7.256 0.960999 7.107 0.960999 6.915C0.962999 6.534 1.004 5.969 1.118 5.289C1.23 4.62 1.433 3.863 1.792 3.228C1.88 3.069 1.978 2.917 2.084 2.777C2.136 2.706 2.191 2.639 2.248 2.574C2.304 2.509 2.362 2.446 2.422 2.387C2.482 2.328 2.543 2.27301 2.606 2.21901C2.669 2.16701 2.733 2.116 2.798 2.07C2.928 1.977 3.063 1.898 3.2 1.832C5.121 0.743004 8.067 0.966005 9.134 1.498Z')
        .size(8)
        .move(x - 4, y - 2)
        .fill('#fff')
        .stroke('#bec3c7');
      // const dot1 = this.draw.circle(2).fill('#f06');
      const overflow = this.draw.rect(this.width, heightItem).attr({
        y: y - (heightItem - heightItem / 2),
        opacity: 0,
        cursor: 'pointer'
      });

      let d = !prev ? `M ${x} ${y} L ${x} ${y}` : `M ${prev.x} ${prev.y} Q ${x1} ${y1} ${x} ${y}`;
      pathBG.attr({ d, stroke: this.mutedColor, fill: 'transparent'});
      path.attr({ d, stroke: color, fill: 'transparent'});

      this.items.push({
        el, title, color, heightItem,
        scrollStatus,
        y1, x1, y, x,
        text, dot, pathBG, path, overflow,
        pathColor: color, dotFill: 'rgba(0, 0, 0, 0)', dotColor: this.mutedColor,
        get strokeLength() {
          return path.node.getTotalLength();
        }
      });
    });

    const dotsGroup = this.draw.group();
    const overflowGroup = this.draw.group();
    this.items.forEach((item, index) => {
      dotsGroup.add(item.dot);
      overflowGroup.add(item.overflow);

      item.overflow.on('mouseover', () => this.animOver(index));
      item.overflow.on('mouseout', () => this.animOut(index));
      item.overflow.on('click', () => {
        scroll.animateScroll(item.el);
      });
    });

    this.render();
    this.scroll();
    this.active = 0;

    window.addEventListener('scroll', this.scroll.bind(this));
  }

  set active(activeIndex) {
    const item = this.items[activeIndex];
    this.items.forEach(item => item.isActive = false);
    this.__activeIndex = activeIndex;
    item.isActive = true;

    this.animOver(activeIndex);

    anime({
      targets: this.items,
      duration: 100,
      easing: 'linear',
      pathColor: (el, i) => item.color,
    })
  }

  get active() {
    return this.__activeIndex;
  }

  scroll(e) {
    let active = 0;
    this.items.forEach((item, index, list) => {
      const progress = item.scrollStatus.progress;
      const { path } = item;

      if (!this.isSafari && index + 1 < list.length) {
        const next = list[index + 1];
        anime.set(next.path.node, {
          strokeDasharray: next.strokeLength,
          strokeDashoffset: next.strokeLength * 2 * (1 - progress) - next.strokeLength,
        });
      }

      if(progress > 0) active = index;
    });

    if (active < 0) active = 0;
    if (active !== this.active) {
      this.active = active;
    }
  }
  animOver(index = this.active, amp = 15) {
    if(this.currentAnimation) this.currentAnimation.pause();

    clearTimeout(this.timerHover);
    this.timerHover = setTimeout(() => this.currentAnimation = anime.timeline({
      targets: this.items,
      easing: 'spring(3, 80, 15, 1)',
      update: () => this.render(),
    }).add({
      x: (el, i) => i === index ? 100 - amp : 100,
      x1: (el, i) => {
        if (Math.abs(i - index) < 5) {
          const sine = (i) => Math.sin((i - 0.5) * 3.55);
          let duration = i - index <= 0;
          let proximity = (Math.abs(i - index));
          proximity = duration ? proximity + 1 : proximity;

          return 100 - sine(proximity) * amp;
        }
      },
    }).add({
      easing: 'easeOutCubic',
      dotFill:  (el, i) => i === index || i === this.active ? el.color : this.isSafari ? '#e0e0e0' :'rgba(0, 0, 0, 0)',
      dotColor: (el, i) => i === index || i === this.active ? el.color : this.mutedColor,
    }, 0), 100);

    return this.currentAnimation;
  }

  animOut() {
    this.animOver(this.active);
  }

  render() {
    this.items.forEach((item, index, items) => {
      const {y1, x1, y, x, text, path, pathBG, dot, pathColor, dotFill, dotColor } = item;
      const prev = items[index - 1];
      let d = `M ${x} ${y} L ${x} ${y}`;
      if (prev) d = `M ${prev.x} ${prev.y} Q ${x1} ${y1} ${x} ${y - 3}`;
      pathBG.attr({ d, style: `stroke-dasharray: ${item.strokeLength - 6}; stroke-dashoffset: -6;` });
      path.attr({ d, stroke: pathColor });
      dot.attr({ fill: dotFill, stroke: dotColor })
        .move(x - 4, y - 2);
      // dot1.attr({ cx: x1, cy: y1 });
      text.move((x - text.length() - 10), y - 5).fill(dotColor);
    });
  }
}

export default Navigation;
