import lottie from 'lottie-web';
import ScrollStatus from "../libs/ScrollStatus";
import Parallax from "../libs/Parallax";

class BackgroundAnimations {
  constructor() {
    this.path = '/media/animations/';
    this.animations = {};
    this.activeAnims = [];
    this.isPaused = false;
    this.containers = [...document.getElementsByClassName('bodymovin')];
    this.breakpoint = this.defineBreakpoint;
    this.breakpoints = {
      'lg': 1920,
      'md': 1280,
      'sm': 768,
      'xs': 320,
    };

    this.containersScrollStatuses = this.containers.map(container => {
      const scrollStatus = new ScrollStatus(container);

      if (scrollStatus.isView) {
        let name = container.dataset.name;
        this.loadAnimation(name, container);
        this.activeAnims.push(this.animations[name]);
      }

      return scrollStatus;
    });

    this.resizeHandler = this.resize.bind(this);
    window.addEventListener('optimizedResize', this.resizeHandler);

    this.scrollHandler = this.scroll.bind(this);
    window.addEventListener('scroll', this.scrollHandler);

    global.bg = this;
  }

  get defineBreakpoint() {
    if (window.innerWidth < 768) return 'xs';
    if (window.innerWidth >= 768 && window.innerWidth < 992) return 'sm';
    if (window.innerWidth >= 980 && window.innerWidth < 1920) return 'md';
    if (window.innerWidth >= 1920) return 'lg';
  }

  loadAnimation(name, container) {
    let breakpoint = this.breakpoints[this.breakpoint];
    if (!!this.animations[name]) {
      this.animations[name].destroy();
    }

    const animation = lottie.loadAnimation({
      name: name,
      container: container,
      renderer: 'svg',
      loop: true,
      autoplay: !this.isPaused,
      path:  `/bodymovin/${breakpoint}/${name}.json`
    });

    animation.addEventListener('DOMLoaded', e => {
      new Parallax(animation.wrapper.querySelector('svg'));

      if (window.hasOwnProperty('preloader')) {
        preloader.setTarget('lottie', true);
      }
    });

    this.animations[name] = animation;
  }

  resize(e) {
    let newBreakpoint = this.defineBreakpoint;

    if (this.breakpoint !== newBreakpoint) {
      this.breakpoint = newBreakpoint;
      for (let name in this.animations) {
          this.loadAnimation(name, this.animations[name].wrapper);
      }
    }
  }

  play() {
    this.isPaused = false;
    this.activeAnims.forEach(item => item.play());
  };

  pause() {
    this.isPaused = true;
    this.activeAnims.forEach(item => item.pause());
  };

  scroll(e) {
    this.activeAnims = [];
    this.containersScrollStatuses.forEach(scrollStatus => {
      let { target, isView, progress } = scrollStatus;

      let name = target.dataset.name;

      if (!this.animations[name] && isView) {
        this.loadAnimation(name, target);
        this.activeAnims.push(this.animations[name]);
      } else if (!!this.animations[name] && isView) {
        if(!this.isPaused) this.animations[name].play();
        this.activeAnims.push(this.animations[name]);
      } else if (!!this.animations[name] && !isView) {
        this.animations[name].pause();
      }
    });
  }

  destroy() {
    for (let name in this.animations) {
      if (this.animations.hasOwnProperty(name)) this.animations[name].destroy();
    }

    window.removeEventListener('optimizedResize', this.resizeHandler);
    window.removeEventListener('scroll', this.scrollHandler);
  }
}

export default BackgroundAnimations;
