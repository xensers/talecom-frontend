/**
 * Optimized Events
 */
export function throttle(type, name, obj) {
  obj = obj || window;
  let running = false;
  let func = function() {
      if (running) { return; }
      running = true;
      requestAnimationFrame(function() {
          obj.dispatchEvent(new CustomEvent(name));
          running = false;
      });
  };
  obj.addEventListener(type, func);
}

export function rem(val) {
  val = typeof val === 'number' ? val : 1;

  return parseInt(getComputedStyle(document.documentElement).fontSize, 10) * val;
}

export function toRem(val) {
  val = typeof val === 'number' ? val : 1;

  return val / parseInt(getComputedStyle(document.documentElement).fontSize, 10);
}

export function relativeOffset(prop, elFrom, elTo) {
  let rectFrom = elFrom.getBoundingClientRect();
  let rectTo   = elTo.getBoundingClientRect();

  return rectTo[prop] - rectFrom[prop];
}

export function rounded(number){
  return Math.ceil(number*100)/100;
}

