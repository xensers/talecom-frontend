const reg = {
  phone: /^(\+\s?7|8)?[\s\-]?[()]?\s?\d{3}\s?[\(\)]?[\s\-]?\d{3}[\s\-]?\d{2}[\s\-]?\d{2}$/,
  login: /^[\wа-яА-Я\s]+$/
};

export default reg;