import SVG from 'svg.js';
import DistortPathAnimate from "./DistortPathAnimate";

export default class SVGClose {
  constructor(ref, duration) {
    const draw = SVG(ref)
      .viewbox({ x: 0, y: 0, width: 83, height: 78 })
      .attr({
       preserveAspectRatio: 'none',
      });

    this.draw = draw;

    this.bgPath = draw.path(`
      M 28.348  1.58913
      C 28.348  1.58913 -7.28199 10.9291  1.34801 40.5891
      C 9.97801 70.2491 17.218   74.5691  37.348  77.5891
      C 57.478  80.6091 92.488   51.4291  80.348  27.5891
      C 67.1081 1.58918 43       -4.00005 28.348  1.58913
      Z
    `);
    this.bgPath.fill("#fff");
    this.animateBG = new DistortPathAnimate(this.bgPath, 4, 900);

    this.iconPath = draw.path(`
      M 26.297 38.61
      C 24.2956 39.9484 24.6958 42.271 26.9108 43.2289
      C 33.7425 45.9321 39.9605 50.1706 46.7922 52.8737
      C 48.807 53.6347 49.8079 50.7478 48.2067 49.7899
      C 42.9761 46.7063 37.7589 43.8067 32.5284 40.7231
      C 34.1429 39.9489 35.7441 39.1875 37.3586 38.4133
      C 39.9605 37.4423 42.5758 37.2589 44.9909 36.1042
      C 46.7922 35.33 45.9916 33.007 44.5905 32.4296
      C 38.9597 30.5007 30.9271 35.5132 26.297 38.61
      Z 
    `);
    this.iconPath.fill('#FFC326');
    if (duration === 'right') {
      this.iconPath.flip('x');
    }
  }

  start() {
    this.animateBG.start();
  }

  stop() {
    this.animateBG.stop();
  }

  clear() {
    this.stop();
    this.draw.clear();
  }
}
