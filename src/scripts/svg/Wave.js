import SVG from 'svg.js';

export default function Wave(el, params = {}) {
  params = Object.assign(params, el.dataset);

  let waveColor      = params.color                    || "#0af";
  let bgColor        = params.bg                       || "none";
  let speed          = parseFloat(params.speed)        || 5;
  let steps          = parseInt(params.steps)          || 5;
  let amplitude      = parseFloat(params.amplitude)    || 0.5;
  let size           = parseFloat(params.size)         || 50;
  let deformationX   = parseFloat(params.deformationX) || 0;
  let deformationY   = parseFloat(params.deformationY) || 0;
  let width          = parseFloat(params.width)        || 768;
  let height         = parseFloat(params.height)       || 100;
  let halfHeight     = height / 2;
  let step           = width / steps;
  let autoplay       = params.hasOwnProperty('autoplay') ? !!params.autoplay : true;
  let dotsRadius     = params.showDots ? 5 : 0;

  let isPaused = !autoplay;
  let t = 0;
  let lastTime = performance.now();

  const draw = SVG(el).size('100%', '100%').viewbox(0, 0, width, height);
  const background = draw.rect(width, height).fill(bgColor);
  const wave    = draw.path();

  wave.fill(waveColor);
   let dots = [];
   for(let i = 0; i <= steps; i++) {
     let c = draw.circle(dotsRadius);

     c.attr({
       fill: i % 2 ? '#f00' : '#00f',
       stroke: '#222',
       'stroke-width': 1,
       'cx': step * i,
       'cy': halfHeight
     });

     c.offsetY = 0;
     c.offsetX = 0;

     dots.push(c);
   }


   function animationLoop() {
     t += performance.now() - lastTime;
     lastTime = performance.now();

     let dotsPath = [];

     dots.forEach((dot, i, dots) => {
       // let y = halfHeight + dot.offsetY;
       let y = (halfHeight - (Math.sin(i + (t * (speed / 10000))) * amplitude) * size) + dot.offsetY;
       let x = step * i + dot.offsetX;

       dot.y(y);
       dot.x(x);

       if (dot.offsetY > 0) {
         dot.offsetY--;
         if (dot.offsetY < 0) dot.offsetY = 0;
       } else if (dot.offsetY < 0) {
         dot.offsetY++;
         if (dot.offsetY > 0) dot.offsetY = 0;
       }

       if (dot.offsetX > 0) {
         dot.offsetX--;
         if (dot.offsetX < 0) dot.offsetX = 0;
       } else if (dot.offsetX < 0) {
         dot.offsetX++;
         if (dot.offsetX > 0) dot.offsetX = 0;
       }

       let res;

       if (i % 2) {
         let d = dots[i - 1];
         res = ['S', d.x(), d.y(), x, y]
       }

       if (res) {
         dotsPath.push(res);
       }
     });

     wave.plot([
       ['M', 0, halfHeight],
       ['L', 0, halfHeight],
       ['Q', 0, halfHeight, 0 - deformationX, halfHeight - deformationY],
       ...dotsPath,
       ['L', width, halfHeight, width + deformationX, halfHeight - deformationY],
       ['L', width, halfHeight],
       ['L', width, height],
       ['L', 0, height],
       ['z']
     ]);

     if (!isPaused) requestAnimationFrame(animationLoop);
   }
   requestAnimationFrame(animationLoop);

   function play() {
     if (isPaused) {
       lastTime = performance.now();
       isPaused = false;
       requestAnimationFrame(animationLoop);
     }
   }

   function pause() {
     if (!isPaused) {
       isPaused = true;
     }
   }

   return {
    play,
    pause,
    get color() { return waveColor },
    set color (val) {
      waveColor = val;
      wave.fill(val);
    },
    get bg() { return bgColor },
    set bg(val) {
      bgColor = val;
      background.fill(bgColor);
    }
   }
}
