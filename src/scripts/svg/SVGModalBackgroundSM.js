import SVG from 'svg.js';
import DistortPathAnimate from "./DistortPathAnimate";

export default class SVGModalBackgroundSM {
  constructor(ref) {
    const draw = SVG(ref)
      .viewbox({ x: 0, y: 0, width: 301, height: 500 })
      .attr({
        preserveAspectRatio: 'none',
      });

    this.draw = draw;

    let path = draw.path([
      [ "M", 165, 0 ],
      [ "C", 0,   1,   4,   78,  0,   244 ],
      [ "C", 4,  400, -3,  495, 149, 495  ],
      [ "C", 292, 495, 306, 416, 300, 187 ],
      [ "C", 299, 50,  275, 0,   165, 0   ],
      [ "Z" ]
    ]);
    path.fill("#fff");

    this.animate = new DistortPathAnimate(path, 10, 1000);
  }
  start() {
    this.animate.start();
  }

  stop() {
    this.animate.stop();
  }

  clear() {
    this.stop();
    this.draw.clear();
  }
}