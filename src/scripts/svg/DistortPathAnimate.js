class DistortPathAnimate {
  constructor(path, amp, duration) {
    this.path     = typeof path === "object" ? path : [];
    this.amp      = typeof amp === "number" ? amp : 10;
    this.duration = typeof duration === "number" ? duration : 1000;
    this.pathArr  = [];
    this.pathTransforms  = [];
    this.timer    = null;
    this.isPaused = true;

    let [width, height] = [path.width(), path.height()];
    path.size(width - this.amp, height - this.amp);
    path.dmove(this.amp / 2, this.amp / 2);

    this.pathArr = path.array().value;
  }

  animate() {
    this.pathTransforms = this.pathArr.map((item, index, arr) => {
      if (index > 0) {
        return item.map((point, pIndex) => {
          if (typeof point === 'number' && !(arr[index + 1][0] === "Z" && pIndex >= 5)) {
            return point + (Math.random() * this.amp * 2 - this.amp);
          } else {
            return point;
          }
        });
      }
      return item;
    });

    this.path.animate(this.duration + 16).plot(this.pathTransforms);

    this.timer = setTimeout(this.animate.bind(this), this.duration);
  }

  start() {
    if (!this.isPaused) return false;
    this.animate();
    this.isPaused = false;
  }

  stop() {
    if (this.isPaused) return true;
    clearTimeout(this.timer);
    this.path.stop();
    this.isPaused = true;
  }
}

export default DistortPathAnimate;
