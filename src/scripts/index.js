import Vue from 'vue';
const VueInputMask = require('vue-inputmask').default;
Vue.use(VueInputMask);

import SmoothScroll from 'smooth-scroll';

import { throttle } from "./helpers";
import scrollStateCssClasses from "./libs/scrollStateCssClasses";
import Parallax from "./libs/Parallax";
import BackgroundAnimations from "./modules/BackgroundAnimations";
import Wave from "./svg/Wave";

import Icon from "../components/Icon";
import Calculators from "../components/Calculators";
import btnRequest from "../components/BtnRequest";
import Modal from "../components/Modal";
import RequestForm from "../components/Forms/RequestForm";
import Services from "../components/Services/index";
import BtnIcon from "../components/BtnIcon";
import ScrollStatus from "./libs/ScrollStatus";

throttle("resize", "optimizedResize");
throttle("scroll", "optimizedScroll");


scrollStateCssClasses();
window.addEventListener('optimizedScroll', scrollStateCssClasses);

const app = new Vue({
  el: '#root',
  components: {
    Calculators,
    btnRequest,
    Modal,
    RequestForm,
    Services,
    Icon,
    BtnIcon
  },

  data: () => ({
    windowWidth: window.innerWidth,
    modal: {
      components: false,
      isOpen: false,
      showControls: false,
      list: false,
      id: 0,
    }
  }),

  mounted() {
    window.addEventListener('resize', e => {
      this.windowWidth = window.innerWidth;
    });


    /* Init Smooth Scroll */
    const scroll = new SmoothScroll('a[href*="#"]', {
      speed: 500,
      speedAsDuration: true
    });

    /* Init Background Animations */
    this.bgAnimations = new BackgroundAnimations();

    /* Preloader target */
    if (window.hasOwnProperty('preloader')) preloader.setTarget('mounted', true);

    /* Init Waves */
    document.querySelectorAll('.wave').forEach(el => {
      const wave = new Wave(el, {
        width: 768,
        height: 100,
        speed: 5,
        steps: 5,
        amplitude: 0.5,
        size: 50,
      });
      const scrollStatus = new ScrollStatus(el);

      window.addEventListener('scroll', () => {
        if (scrollStatus.isView) {
          wave.play();
        } else {
          wave.pause();
        }
      })
    })
  },

  watch: {
    'modal.isOpen' : function (val, old) {
      if (val) {
        this.bgAnimations.pause();
      } else {
        this.bgAnimations.play();
      }
    }
  },

  methods: {
    openModal( component = false, showControls = false, id = 0 ) {
      if (component instanceof Array) {
        this.modal.list = component;
        this.modal.component = false;
      } else {
        this.modal.list = false;
        this.modal.component = component;
      }
      this.modal.showControls = showControls;
      this.modal.id = id;
      this.modal.isOpen = true;
    },
    closeModal() {
      this.$refs.Modal.close();
      this.bgAnimations.play();
    }
  }
});
