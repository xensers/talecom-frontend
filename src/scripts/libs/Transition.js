export default class Transition {
  constructor(el, name = 'transition', duration = 1000, hooks = {}) {
    if (el === undefined) {
      throw "Argument el is required!";
    }

    if (typeof el === "string") {
      el = document.querySelector(el);
    }

    if (!(el instanceof Element)) {
      throw "el is not instance of Element";
    }

    let hooksProperties = [
      'beforeEnter',
      'enter',
      'afterEnter',
      'enterCancelled',
      'beforeLeave',
      'leave',
      'afterLeave',
      'leaveCancelled'
    ];

    hooksProperties.forEach(key => {
      hooks[key] = (!hooks.hasOwnProperty(key) || typeof hooks[key] !== "function") ? function(){} : hooks[key];
    });

    this.el       = el;
    this.name     = name;
    this.duration = duration;
    this.hooks    = hooks;

    this.__timerEnter = null;
    this.__timerLeave = null;
  }

  enter() {
    this.el.style.display = 'block';
    this.el.classList.add(`${this.name}-enter`);
    this.el.classList.add(`${this.name}-enter-active`);

    this.hooks.beforeEnter(this);
    requestAnimationFrame(() => {
      this.el.classList.remove(`${this.name}-enter`);
      this.el.classList.add(`${this.name}-enter-to`);

      this.hooks.enter(this);
    });

    this.__timerEnter = setTimeout(() => {
      this.el.classList.remove(`${this.name}-enter-active`);
      this.el.classList.remove(`${this.name}-enter-to`);

      this.hooks.afterEnter(this);
    }, this.duration);
  }

  enterCancell() {
    clearTimeout(this.__timerEnter);
    this.el.classList.remove(`${this.name}-enter`);
    this.el.classList.remove(`${this.name}-enter-active`);
    this.el.classList.remove(`${this.name}-enter-to`);
    this.hooks.enterCancelled();
  }

  leave() {
    this.el.classList.add(`${this.name}-leave`);
    this.el.classList.add(`${this.name}-leave-active`);

    this.hooks.beforeLeave(this);
    requestAnimationFrame(() => {
      this.el.classList.remove(`${this.name}-leave`);
      this.el.classList.add(`${this.name}-leave-to`);

      this.hooks.leave(this);
    });

    this.__timerLeave = setTimeout(() => {
      this.el.style.display = 'none';
      this.el.classList.remove(`${this.name}-leave-active`);
      this.el.classList.remove(`${this.name}-leave-to`);

      this.hooks.afterLeave(this);
    }, this.duration);
  }

  leaveCancell() {
    clearTimeout(this.__timerLeave);
    this.el.classList.remove(`${this.name}-leave`);
    this.el.classList.remove(`${this.name}-leave-active`);
    this.el.classList.remove(`${this.name}-leave-to`);
    this.hooks.leaveCancelled();
  }
}
