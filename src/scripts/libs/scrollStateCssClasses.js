let lastPageYOffset = window.pageYOffset;

function scrollStateCssClasses() {
  let pageYOffsetBottom = document.documentElement.offsetHeight - window.innerHeight - window.pageYOffset;

  if(window.pageYOffset < 50) {
    document.documentElement.classList.add('scroll--page-top');
  } else {
    document.documentElement.classList.remove('scroll--page-top');
  }

  if(window.pageYOffset < 320) {
    document.documentElement.classList.add('scroll--first-screen');
  } else {
    document.documentElement.classList.remove('scroll--first-screen');
  }

  if(pageYOffsetBottom < 20) {
    document.documentElement.classList.add('scroll--page-bottom');
  } else {
    document.documentElement.classList.remove('scroll--page-bottom');
  }

  if(lastPageYOffset < window.pageYOffset) {
    document.documentElement.classList.add('scroll--down');
    document.documentElement.classList.remove('scroll--up');
  } else {
    document.documentElement.classList.remove('scroll--down');
    document.documentElement.classList.add('scroll--up');
  }

  lastPageYOffset = window.pageYOffset;
}

export default scrollStateCssClasses;