import ScrollStatus from "./ScrollStatus";

class Parallax {
   constructor(wrapper) {
     this.scrollStatus = new ScrollStatus(wrapper);
     this.layers = wrapper.querySelectorAll('.parallax');
     this.parallaxLeyes = [];
     this.__progressX = 0.5;
     this.__progressY = 0.5;

     this.layers.forEach((item, i, arr) => {
       let classList = item.classList.value;
       let execY = /y(-?\d+)/.exec(classList);
       let execX = /x(-?\d+)/.exec(classList);

       this.parallaxLeyes.push({
         el: item,
         y: execY ? +execY[1] : 0,
         x: execX ? +execX[1] : 0,
       });
     });

     window.addEventListener('mousemove', this.onMouseMove.bind(this));
     window.addEventListener('scroll', this.onScroll.bind(this));
   }

   set progressX(progress) {
     this.__progressX = progress;

     this.render();
   }

   set progressY(progress) {
     this.__progressY = progress;

     this.render();
   }

  onMouseMove(e) {
    if (this.scrollStatus.isView) {
      this.progressX = e.clientX / window.innerWidth;
      // this.progressY = e.clientY / window.innerHeight;
    }
  }

  onScroll(e) {
    if (this.scrollStatus.isView) {
      this.progressY = this.scrollStatus.progress;
    }
  }

   render() {
      requestAnimationFrame(t => {
         this.parallaxLeyes.map(layer => {
           let y = layer.y * 2 * this.__progressY - layer.y;
           let x = layer.x * 2 * this.__progressX - layer.x;

           layer.el.style.transform = `translate(${x.toFixed(2)}px, ${y.toFixed(2)}px)`;
         })
      });
   }
}

export default Parallax;
