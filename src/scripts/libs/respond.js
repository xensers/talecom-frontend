const respond = {
    to(breakpoint, callback, runWhenCalled, addEvent) {
        let active = false;
        if (breakpoint >= window.innerWidth) {
            active = true;
            if (runWhenCalled) {
                requestAnimationFrame(callback);
            }
        }

        if (addEvent) {
            window.addEventListener("optimizedResize", function () {
                if (!active) {
                    active = true;
                    if (breakpoint >= window.innerWidth) {

                        setTimeout(function () {
                            requestAnimationFrame(callback);
                        }, 100);
                        return true;
                    }
                }
                if (breakpoint <= window.innerWidth) active = false;
            });
        }
    },
    from(breakpoint, callback, runWhenCalled, addEvent) {
        let active = false;
        if (breakpoint <= window.innerWidth) {
            active = true;
            if (runWhenCalled) {
                requestAnimationFrame(callback);
            }
        } else {
        }

        if (addEvent) {
            window.addEventListener("optimizedResize", function() {
                if (!active) {
                    active = true;
                    if (breakpoint <= window.innerWidth) {

                        setTimeout(function(){
                            requestAnimationFrame(callback);
                        }, 100);
                        return true;
                    }
                }
                if (breakpoint >= window.innerWidth) active = false;
            });
        }
    }
};

export default respond;