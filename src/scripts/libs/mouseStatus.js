let mouseStatus = null;

document.body.addEventListener('mousemove', function(e){
  mouseStatus = e;
});

document.body.addEventListener('click', function(e){
  mouseStatus = e;
});

export default () => (mouseStatus);
