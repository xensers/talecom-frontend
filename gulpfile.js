const { task, watch, series, parallel, src, dest } = require('gulp'),
      browserSync = require('browser-sync').create();
      gutil       = require( 'gulp-util' );
      ftp         = require( 'vinyl-ftp' );

const rename = require('gulp-rename'),
      cache  = require('gulp-cache'),
      del    = require('del');

const pug = require('gulp-pug'),
      minifyInline = require('gulp-minify-inline');

const html2pug = require('gulp-html2pug');

const imagemin    = require('gulp-imagemin'),
      imgCompress = require('imagemin-jpeg-recompress');

const sass         = require('gulp-sass'),
      aliases      = require('gulp-style-aliases'),
      sourcemaps   = require('gulp-sourcemaps'),
      cssnano      = require('gulp-cssnano'),
      autoprefixer = require('gulp-autoprefixer');

const webpack       = require('webpack'),
      webpackStream = require('webpack-stream'),
      webpackConfig = require('./webpack.config.js'),
      uglify        = require('gulp-uglify');

const named   = require('vinyl-named'),
      through = require('through2');

const plumber = require('gulp-plumber'),
      notify  = require('gulp-notify');

const VueLoaderPlugin = require('vue-loader/lib/plugin');

const errorHandler = notify.onError('<%= error.message %>');

let isDev = process.env.NODE_ENV === 'development';

const paths = {
  pug: {
    src: 'src/views/*.pug',
    dest: 'dist/',
    watch: ['src/views/**/*.pug', 'src/preloader/**/*'],
  },
  styles : {
    src: 'src/styles/*.scss',
    dest: 'dist/styles/',
    watch: 'src/styles/**/*',
  },
  scripts : {
    src: 'src/scripts/index.js',
    dest: 'dist/scripts/',
  },
  images : {
    src: 'src/images/**/*',
    dest: 'dist/images/',
    watch: 'src/images/**/*',
  },
  bodymovin: {
    src: 'src/bodymovin/**/*.json',
    dest: 'dist/bodymovin/',
    staticWatch: 'src/bodymovin/'
  },
  static : {
    src: 'static/**/*',
    dest: 'dist/',
    staticWatch: 'static/'
  },
  html2pug : {
    src: 'utility/html2pug/input/**/*.html',
    dest: 'utility/html2pug/output/',
    watch: 'utility/html2pug/input/**/*.html'
  }
};

const conn = ftp.create( {
  host:     'fortuna.timeweb.ru',
  user:     'cs17145',
  password: 'L5MqvvzMRpWT',
  parallel: 10,
  log:      gutil.log
} );

function deploy(globs) {
  return src( globs, { base: 'dist/', buffer: false } )
    .pipe( conn.newer( '/public_html'))
    .pipe( conn.dest( '/public_html'))
}

task('deploy', function () {
  return deploy([
    'dist/**'
  ]);
});


task('serve', function() {
  browserSync.init({
      server: {
          baseDir: "dist/"
      }
  });
});

task('scripts', cb => src(paths.scripts.src)
  .pipe(plumber({errorHandler}))
  .pipe(named())
  .pipe(webpackStream( webpackConfig ))
  .pipe(dest(paths.scripts.dest))
  .pipe(browserSync.stream())
);

task('styles', cb => src(paths.styles.src)
    .pipe(plumber({errorHandler}))
    .pipe(aliases({
      "~": "./node_modules/",
      "@": "./src/"
    }))
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'expanded'}))
    .pipe(autoprefixer())
    .pipe(dest(paths.styles.dest))
    .pipe(cssnano({
        reduceIdents: false
     }))
    .pipe(rename({ suffix: '.min' }))
    .pipe(sourcemaps.write('./maps'))
    .pipe(dest(paths.styles.dest))
    .pipe(browserSync.stream())
);

task('pug', cb => src(paths.pug.src)
  .pipe(plumber({errorHandler}))
  .pipe(pug({
    pretty: true
   }))
  .pipe(minifyInline())
  .pipe(dest(paths.pug.dest))
  .pipe(browserSync.stream())
);

task('images', cb => src(paths.images.src)
  .pipe(plumber({errorHandler}))
  .pipe(cache(imagemin([
    imgCompress({
      loops: 4,
      min: 70,
      max: 80,
      quality: 'high'
    }),
    imagemin.gifsicle(),
    imagemin.optipng(),
    imagemin.svgo()
  ])))
  .pipe(dest(paths.images.dest))
  .pipe(browserSync.stream())
);

task('static', cb => src(paths.static.src)
  .pipe(plumber({errorHandler}))
  .pipe(dest(paths.static.dest))
  .pipe(browserSync.stream())
);

task('bodymovin', cb => src(paths.bodymovin.src)
  .pipe(plumber({errorHandler}))
  .pipe(dest(paths.bodymovin.dest))
  .pipe(browserSync.stream())
);

task('html2pug', cb => src(paths.html2pug.src)
  .pipe(plumber({errorHandler}))
  .pipe(html2pug({ fragment: true }))
  .pipe(dest(paths.html2pug.dest))
);

task('watch', cb => {
  console.log('Watch for:');
  console.group();
  for (let key in paths) {
    if (paths[key].hasOwnProperty('watch')) {
      let path = paths[key].watch;
      let watcher = watch(path, series(key));

      console.log('- ', path);

      watcher.on('all', function(stats, path) {
         console.log(`File ${path} was ${stats}`);
      });
    }

    if (paths[key].hasOwnProperty('staticWatch')) {
      let watcherStatic = watch(paths[key].src);
      let base = paths[key].staticWatch;
      console.log('- ', paths[key].src);
      watcherStatic.on('all', (stats, path) => {
        console.log(`File ${path} was ${stats}`);

        if (stats === 'add' || stats === 'change') {
          src(path, {base: base, buffer: false})
            .pipe(dest(paths[key].dest))
            .pipe(browserSync.stream())
        }
      });
    }
  }

  console.groupEnd();
  return cb();
});

task('clean', cb => del('dist'));
task('clear', cb => cache.clearAll());

task('build', series('clean', parallel('pug', 'scripts', 'styles', 'images', 'bodymovin', 'static')));


exports.default = parallel('watch', 'serve', 'scripts');
