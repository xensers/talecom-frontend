# Начало работы

Для начала работы над проектом необходимо [скачать архив](https://gitlab.com/xensers/talecom-frontend/-/archive/master/talecom-frontend-master.zip) или клонировать текущий репозиторий:

```bash
git clone https://gitlab.com/xensers/talecom-frontend.git
```

Далее следует установить npm-библиотеки:

```bash
yarn install
```

По завершении можно запускать сборку:

```bash
yarn dev
```

Что бы собрать версию для продакшина выполнить:

```bash
yarn build
```
В папке `dist` появится продакшн версия верстки со сжатыми картинакми, собранными в один файл и сжатыми стилями, скриптами.
